import unittest
from mongoDbAdapter import mongoDbAdapter


class test_funcs(unittest.TestCase):
 
    def test_init(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.__init__(self,uri="wrong uri"),None)

    def test_test_connection(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertTrue(mongoDbAdapter.test_connection(self))
        self.assertEqual(mongoDbAdapter.__init__(self,uri="wrong uri"),None)
        self.assertEqual(mongoDbAdapter.test_connection(self),"Server not available")
        

    def test_get_all_databases(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertListEqual(mongoDbAdapter.get_all_databases(self),['admin', 'config', 'local', 'test1', 'test2'])
    
    def test_get_all_user_databases(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertListEqual(mongoDbAdapter.get_all_user_databases(self),['test1', 'test2'])

    def test_get_all_db_collections(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertListEqual(mongoDbAdapter.get_all_db_collections(self,'test1'),['c1', 't1'])

#its not working fine
    def test_find_in_collection(self):
    
        result = "[{'_id':ObjectId('5b307995d1e1dc05343c8639'),'color': 'shiny blue','type': 'dog','weight':  35}]"
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(type(mongoDbAdapter.find_in_collection(self,database='test1',collection='t1',query={"type":"dog","color":"shiny blue"})),list)

#its not working fine        
    def test_get_collection_keys(self):
        result = str("[dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'citizen', 'job', 'name', 'ph no', 'resident']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color']), dict_keys(['_id', 'type', 'weight', 'color'])]")
        self.maxDiff=None
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(type(mongoDbAdapter.get_collection_keys(self,'test1','t1')),list)

#its not working fine
    def test_get_all_documents(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(type(mongoDbAdapter.get_all_documents(self,'test1','t1')),list)

#its not working fine
    def test_get_n_documents(self):
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(type(mongoDbAdapter.get_n_documents(self,'test1','t1',2)),list)


    def test_insert_document(self):
        query={ 'citizen': 'Indian','job': 'student','name': 'aman','ph no': '9454569822','resident': 'Delhi'}

        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.insert_document(self,'test1','c1',query),'entry successfull')

    def test_insert_many_documents(self):
        query =[{"type": "cat", "weight": 25, "color": "orange"},
                {"type": "dog", "weight": 35, "color": "black"},
                {"type": "cat", "weight": 10, "color": "black"}
                ]

        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.insert_many_documents(self,'test1','c1',query),'entry successfull')

    def test_update_one_document(self):
        query={ 'citizen': 'Indian','job': 'student','name': 'aman','ph no': '9454569822','resident': 'Delhi'}

        update ={"type": "cat", "weight": 25, "color": "orange"}

        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.update_one_document(self,'test1','c1',query,update),'update successfull')

    def test_update_many_document(self):
        query={ 'citizen': 'Indian','job': 'student','name': 'aman','ph no': '9454569822','resident': 'Delhi'}

        update ={"type": "cat", "weight": 25, "color": "orange"}

        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.update_many_document(self,'test1','c1',query,update),'update successfull')

    def test_delete_one_document(self):
        query = {"type": "cat", "weight": 25}
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.delete_one_document(self,'test1','t1',query),"delete successfull")

    def test_delete_many_document(self):
        query = {"type": "dog"}
        self.assertEqual(mongoDbAdapter.__init__(self,uri="mongodb://localhost"),None)
        self.assertEqual(mongoDbAdapter.delete_one_document(self,'test1','t1',query),"delete successfull")


'update successfull'
if __name__ == "__main__":
    unittest.main()