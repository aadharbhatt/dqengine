import pymongo as pm
import mongoDbAdapter

obj1 = mongoDbAdapter.mongoDbAdapter(uri="mongodb://localhost")

a = obj1.test_connection()
#print(a)


b = obj1.get_all_user_databases()
#print(b)


a = obj1.get_all_databases()
#print(a)

a = obj1.find_in_collection(database='test1',collection='t1',query={"type":"dog","color":"shiny blue"})
#print(a)


a = obj1.get_all_db_collections('test1')
#print(a)


a = obj1.get_collection_keys('test1','t1')
#print(a,'\n')

a = obj1.get_all_documents('test1','t1')
#print(a,'\n')

obj1.get_n_documents('test1','t1',2)

query={ 'citizen': 'Indian','job': 'student','name': 'aman','ph no': '9454569822','resident': 'Delhi'}
a = obj1.insert_document('test1','t1',query)
#print(a)

query=[{"type": "cat", "weight": 25, "color": "orange"},
        {"type": "dog", "weight": 35, "color": "black"},
        {"type": "cat", "weight": 10, "color": "black"}
        ]
obj1.insert_many_documents('test1','t1',query)


query={ 'citizen': 'Indian','job': 'student','name': 'aman','ph no': '9454569822','resident': 'Delhi'}
update ={"type": "cat", "weight": 25, "color": "orange"}

a = obj1.update_one_document('test1','t1',query,update)
print(a)

a = obj1.delete_one_document('test1','t1',{"type": "cat", "weight": 25})
print(a)



