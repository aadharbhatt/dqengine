import pymongo
from NoSqlAdapter import NoSqlAdapter
from pprint import pprint

class mongoDbAdapter(NoSqlAdapter):

    def __init__(self,uri:str):
        """
        sets up connection between script and mongoDB 
        """
               
        self.client = pymongo.MongoClient(uri)
        try:
            # The ismaster command is cheap and does not require auth.
            self.client.admin.command('ismaster')
           
        except pymongo.errors.ConnectionFailure:
            print("Server not available")
                        
    def test_connection(self):
        """
        Test the server connection.
        :return: True - If connection stableased else False
        """
        try:
             # The ismaster command is cheap and does not require auth.
            info = self.client.admin.command('ismaster')
            return info['ismaster']
        except pymongo.errors.ConnectionFailure:
            return "Server not available"

    def get_all_databases(self):
        """
        Lists all the available databases on connected instance.
         ------------------------------------------------------------------------------
        Usages:
            for r in s.get_all_databases():
                print r[0]
        ------------------------------------------------------------------------------
        :return: generator - tuples or raise exception
        """
        try:
            return self.client.database_names()
        except:
            return 'error getting list of available databases on connected instance'

    def get_all_user_databases(self):
        """
        Lists all the available user databases (ignore system) on connected instance.
         ------------------------------------------------------------------------------
        Usages:
            for r in s.get_all_user_databases():
                print r
        ------------------------------------------------------------------------------
        :return: generator - tuples or raise exception
        """

        try:
            user_db = self.client.database_names()
            return user_db[3:]
        except:
            return 'error getting list of available databases on connected instance'

    def get_all_db_collections(self, database:str):
        """
        List all the collections in a perticular database.
        :return: dictionary (Database: [List of table]) or raise exception
        """
        
        self.db = self.client[str(database)]
        collection = self.db.collection_names()

        return collection

    def find_in_collection(self,database:str, collection:str, query:'dictionary'):
        """
        finds the given query in current DB's collection
        """
        list_of_doc = []
        self.dbz = self.client[str(database)]
        clcn = self.dbz[str(collection)]
        for post in clcn.find(query):
            list_of_doc.append(post)

        return list_of_doc

    def get_collection_keys(self, database:str, collection:str):
        """
        Returns list of column names.
        :param full_table_name:
        :return: list of string in format 'Column Name (Data Type)' or raise exception.
        """
        list_of_keys = []
        self.db = self.client[str(database)]
        collection = self.db[str(collection)]

        cursor = collection.find({})
        for document in cursor:
            list_of_keys.append(document.keys())

        return list_of_keys


    def get_all_documents(self, database:str, collection:str):
        """
        List all the documents of a perticular collection.
        :return: generator - BSON or raise exception
        """
        list_of_documents = []
        self.db = self.client[str(database)]
        collection = self.db[str(collection)]

        cursor = collection.find({})
        for document in cursor:
            list_of_documents.append(document)

        return list_of_documents        

    def get_n_documents(self,  database:str, collection:str, N:int):
        """
        Returns the requested number of rows.
        :param N: number of requested documents
        :return: List of values(list) or raise exception
        """
        list_n_docs = []
        self.db = self.client[str(database)]
        collection = self.db[str(collection)]

        cursor = collection.find(limit=N)
        for document in cursor:
            list_n_docs.append(document)

        return list_n_docs
        

    def insert_document(self, database:str, collection:str, query:'input a BSON dictionary'):
        """
        Inserts single row
        usage : query = {
                        "id": "1234",
                        "name":"abcd",
                        "age":"55",
                        "country":"India"
                        }
        """
        try:

            self.db = self.client[str(database)]
            collection = self.db[str(collection)]
    
            collection.insert_one(query)
            return ('entry successfull')
        except pymongo.errors.BSONError as error:
            return('error => ',error)


    def insert_many_documents(self, database:str, collection:str, query:'input a BSON dictionary'):
        """
        Inserts many row
        usage : query =[
                        {"type": "cat", "weight": 25, "color": "orange"},
                        {"type": "dog", "weight": 35, "color": "black"},
                        {"type": "cat", "weight": 10, "color": "black"}
                        ]
        """
        try:

            self.db = self.client[str(database)]
            collection = self.db[str(collection)]
            
            collection.insert_many(query)
            return('entry successfull')
        except pymongo.errors.BSONError as error:
            print('error => ',error)

    def update_one_document(self, database:str, collection:str, query:'input a BSON dictionary',update:'input a BSON to update'):
        try:
            self.db = self.client[str(database)]
            self.collection = self.db[str(collection)]
            
            self.collection.update_one(query,{"$set":update})
            return ('update successfull')
        except pymongo.errors.BSONError as error:
            print('error => ',error)

    def update_many_document(self, database:str, collection:str, query:'input a BSON dictionary',update:'input a BSON to update'):
        try:
            self.db = self.client[str(database)]
            collection = self.db[str(collection)]
            
            collection.update_many(query,{"$set":update})
            return('update successfull')
        except pymongo.errors.BSONError as error:
            print('error => ',error)


    def delete_one_document(self, database:str, collection:str, query:'input a BSON dictionary'):
        try:
            self.db = self.client[str(database)]
            collection = self.db[str(collection)]
            
            collection.delete_one(query)
            return('delete successfull')
        except pymongo.errors.BSONError as error:
            print('error => ',error)

    def delete_many_document(self, database:str, collection:str, query:'input a BSON dictionary'):
        try:
            self.db = self.client[str(database)]
            collection = self.db[str(collection)]
            
            collection.delete_many(query)
            print('delete successfull')
        except pymongo.errors.BSONError as error:
            print('error => ',error)

    